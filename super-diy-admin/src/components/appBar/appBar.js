import React from 'react';
import { Layout,Menu,Row,Col,Button } from 'antd';
import { BaseComponent,Global } from '../../base';
import {action} from 'mobx';
import './appBar.less';
const { Header } = Layout;
export default class TopBar extends BaseComponent{
    @action.bound
    _loginHandler(){
        Global.set("showUserLogin",true);
    }
    render(){
        return (
            <Header className="appBar">
                <Row type="flex" justify="end">
                    <Col span={2}>
                        <Button onClick={this._loginHandler} className='loginBtn'>登录</Button>
                    </Col>
                </Row>
            </Header>
        )
    }
}