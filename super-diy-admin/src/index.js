import React , { Component } from 'react';
import ReactDOM from 'react-dom';
import {action} from 'mobx';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import o_pages from './pages/';
import { Spin,Modal } from 'antd';
import {Global,BaseComponent} from './base';
import './index.less';
Global.set("showUserLogin",true);
let a_pages = [];
for(let [name,page] of Object.entries(o_pages)){
    Global.set(name,{});
    a_pages.push(<Route key={name} path={`/${name}`} component={page}/>);
}
class App extends BaseComponent {
    @action.bound
    _handleCancel(){
        Global.set('showUserLogin',false);
    }
    render() {
        return (
            <Router>
                <div className="sd-content">
                    {a_pages}
                    {/*<Spin className='sd-loding' size="large"/>*/}
                    <Modal title={
                            <div className='loginTitle'>
                                登 录
                            </div>
                        }
                        visible={Global.get('showUserLogin')}
                        footer={null}
                        maskClosable={true}
                        onCancel={this._handleCancel}
                    >
                        <div className='loginCon'>
                            <img src="https://gss0.bdstatic.com/94o3dSag_xI4khGkpoWK1HF6hhy/baike/w%3D268%3Bg%3D0/sign=7bcb659c9745d688a302b5a29cf91a23/2934349b033b5bb571dc8c5133d3d539b600bc12.jpg"/>
                        </div>
                    </Modal>
                </div>
            </Router>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
